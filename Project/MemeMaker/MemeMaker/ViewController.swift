//
//  ViewController.swift
//  MemeMaker
//
//  Created by Keo Shiko on 2020/12/29.
//  Copyright © 2020 Glucode. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var topCaptionSegmentedControl: UISegmentedControl!
    
    @IBOutlet weak var bottomCaptionSegmentedControl: UISegmentedControl!
    
    
    @IBOutlet weak var topCaptionLabel: UILabel!
    
    
    @IBOutlet weak var bottomCaptionLabel: UILabel!
    
    
    @IBAction func updateChanges(_ sender: UISegmentedControl) {
        setLabels()
    }
    
    
    
    var topChoices : [(CaptionChoice)] = []
    var bottomChoices : [(CaptionChoice) ] = []
    
    
    
    func setLabels() {
        topCaptionLabel.text = topChoices[topCaptionSegmentedControl.selectedSegmentIndex].caption
        
        bottomCaptionLabel.text = bottomChoices[bottomCaptionSegmentedControl.selectedSegmentIndex].caption
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        let topChoice1 = CaptionChoice(emoji: "🕶", caption: "you know what's cool??" )

        topChoices.append(topChoice1)
        
        let topChoice2 = CaptionChoice(emoji: "🎮", caption: "Phanda, Pusha, Play?" )

        topChoices.append(topChoice2)
        
        let topChoice3 = CaptionChoice(emoji: "🎯", caption: "If you dont shoot, you don't score?")

        topChoices.append(topChoice3)
        
        
        
        let bottomChoice1 = CaptionChoice(emoji: "🔞", caption: "Viewers discretion is advised")

        bottomChoices.append(bottomChoice1)
        
        let bottomChoice2 = CaptionChoice(emoji:"🏧",caption: "mo money, mo problems")

        bottomChoices.append(bottomChoice2)
        
        let bottomChoice3 = CaptionChoice(emoji: "☠️", caption: "Covid-19")

        bottomChoices.append(bottomChoice3)
        
        
        
        
        topCaptionSegmentedControl.removeAllSegments()
        
        for choice in topChoices {
            topCaptionSegmentedControl.insertSegment(withTitle: choice.emoji, at: topChoices.count, animated: false)
        }
        
        topCaptionSegmentedControl.selectedSegmentIndex = 0
        
        
        
        bottomCaptionSegmentedControl.removeAllSegments()
        
        for choice in bottomChoices {
            bottomCaptionSegmentedControl.insertSegment(withTitle: choice.emoji, at: bottomChoices.count, animated: false)
        }
        
        bottomCaptionSegmentedControl.selectedSegmentIndex = 0
        
        setLabels()
    }

    

}

