//
//  ViewController.swift
//  RPS
//
//  Created by Keo Shiko on 2020/12/24.
//  Copyright © 2020 Glucode. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var appSign: UILabel!
    @IBOutlet weak var statusOfTheGame: UILabel!
    
    @IBOutlet weak var rockSign: UIButton!
    @IBOutlet weak var paperSign: UIButton!
    @IBOutlet weak var scissorSign: UIButton!
    
    
    @IBOutlet weak var playAgain: UIButton!
    
    
    @IBAction func playRock(_ sender: UIButton) {
        play(.Rock)
    }
    
    @IBAction func playPaper(_ sender: UIButton) {
        play(.Paper)
    }
    
    @IBAction func playScissors(_ sender: UIButton) {
        play(.Scissors)
    }
    
    @IBAction func playAgain(_ sender: UIButton) {
        updateUI(.Start)
    }
    
    
    
    func updateUI(_ state: GameState) {
        
        var status : String {
            switch state {
            case .Start : return "Rock, Paper, Scissors???"
            case .Draw : return "It's a bore draw 🤨"
            case .Lose : return "Your Loss! 🤡"
            case .Win : return "YOU WIN!!!🥳"
            }
        }
        
        statusOfTheGame.text = status
        
        if state == .Start {
            view.backgroundColor = .darkGray
            
            appSign.text = "🤖"
            playAgain.isHidden = true
            
            rockSign.isEnabled = true
                paperSign.isEnabled = true
                    scissorSign.isEnabled = true
            
            rockSign.isHidden = false
            paperSign.isHidden = false
            scissorSign.isHidden = false
            
            
            
        }else if state == .Draw {
            view.backgroundColor = .brown
            
            rockSign.isEnabled = false
                paperSign.isEnabled = false
                    scissorSign.isEnabled = false
            
            playAgain.isHidden = false
        } else if state == .Lose {
            view.backgroundColor = .red
            
            
            playAgain.isHidden = false
        }
        else {
            view.backgroundColor = .green
            
            
            playAgain.isHidden = false
        }
        
        
        
    }
    
    func play(_ sign: Sign){
      
        let appTurn = randomSign()
        
        
        updateUI(sign.play(appTurn))
        
        appSign.text = appTurn.emoji
        
        rockSign.isEnabled = true
            paperSign.isEnabled = true
                scissorSign.isEnabled = true
        
        
        if sign == .Paper {
            paperSign.isHidden = false
            rockSign.isHidden = true
            scissorSign.isHidden = true
        } else if sign == .Rock {
            paperSign.isHidden = true
            rockSign.isHidden = false
            scissorSign.isHidden = true
        } else if sign == .Scissors {
            paperSign.isHidden = true
            rockSign.isHidden = true
            scissorSign.isHidden = false
        }
        
        
        playAgain.isEnabled = true
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        updateUI(.Start)
    }


}

