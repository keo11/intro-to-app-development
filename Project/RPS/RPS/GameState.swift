//
//  GameState.swift
//  RPS
//
//  Created by Keo Shiko on 2020/12/24.
//  Copyright © 2020 Glucode. All rights reserved.
//

import Foundation


enum GameState  {
    case Start, Win, Lose, Draw
    

}
