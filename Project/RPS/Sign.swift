//
//  Sign.swift
//  RPS
//
//  Created by Keo Shiko on 2020/12/24.
//  Copyright © 2020 Glucode. All rights reserved.
//

import Foundation
import GameplayKit

let randomChoice = GKRandomDistribution(lowestValue: 0, highestValue: 2)

func randomSign() -> Sign {
    
    let sign = randomChoice.nextInt()
    
    if sign == 0 {
        return .Rock
    } else if sign == 1 {
        return .Paper
    } else {
        return .Scissors
    }
    
}

enum Sign {
    case Rock, Paper, Scissors
    
    var emoji: String {
        switch self {
        case .Rock: return "👊🏾"
        case .Paper: return "✋🏾"
        case .Scissors: return "✌🏾"
        }
    }
    
    func play (_ playerChoice: Sign) -> GameState {
        switch self {
        case .Rock:
            if playerChoice == .Paper {
                return .Lose
            }
            else if playerChoice == .Rock {
                return .Draw
            }
            else if playerChoice == .Scissors {
                return .Win
            }
            
        case .Paper :
            if playerChoice == .Paper {
                return .Draw
            }
            else if playerChoice == .Rock {
                return .Win
            }
            else if playerChoice == .Scissors {
                return .Lose
            }
            
        case .Scissors :
            if playerChoice == .Paper {
                return .Win
            }
            else if playerChoice == .Rock {
                return .Lose
            }
            else if playerChoice == .Scissors {
                return .Draw
            }
            
        }
        return .Start
        
    }
}
