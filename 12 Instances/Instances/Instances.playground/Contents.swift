class Car {
    var fuelLevel : Int
    var mileage :Int
    var name : String
    var location = ""
    
    
    init(_ fuel:Int,_ mileage:Int, _ name : String) {
        self.fuelLevel = fuel
        self.mileage = mileage
        self.name = name
    }
    
    func startCar(){
        fuelLevel -= 1
        
        checkFuel()
    }
    
    func checkFuel() {
        if(fuelLevel > 5) {
            print("Good to go!")
        }
        else if (fuelLevel < 5 && fuelLevel > 0) {
            print("Low fuel! Please get more gas!!!")
        }
        else {
            print("Fuel empty!! Cannot start car :( ")
        }
    }
    
    func topUpFuel(value : Int) {
        fuelLevel += value
        print("added more fuel \(value)")
        if(fuelLevel > 10) {
            fuelLevel = 10
        }
    }
    
    func move() {
        mileage += 10
        fuelLevel -= 2
        if(mileage % 100 == 0) {
            print("Car service due! Please service your car soon")
        }
        
        print("we moving! Mileage is now - \(mileage)")
        
    }
    
    
    
}

let benzo = Car(3,90,"Benz")
let bmw = Car(2,80,"bmw")
let audi = Car(4,60,"A4")
let vw = Car(2, 90,"GTI")
let datsun = Car(3,70,"Go")

print("Volkswagen")
vw.startCar()
vw.topUpFuel(value: 8)
vw.move()
vw.move()
vw.move()
vw.location = "Mabopane"

print("")
print("")
print("Mercedes")
benzo.startCar()
benzo.topUpFuel(value: 8)
benzo.move()
benzo.move()
benzo.move()
benzo.location = "Soshanguve"

print("")
print("")
print("Bavaria Motor Works")
bmw.startCar()
bmw.topUpFuel(value: 6)
bmw.move()
bmw.move()
bmw.move()
bmw.location = "Ga-Rankuwa"

print("")
print("")
print("Audi")
audi.startCar()
audi.topUpFuel(value: 7)
audi.move()
audi.move()
audi.move()
audi.location = "Mamelodi"

print("")
print("")
print("Datsun")
datsun.startCar()
datsun.topUpFuel(value: 5)
datsun.move()
datsun.move()
datsun.move()
datsun.location = "Hamanskraal"

class VehicleTransporter {
    var location : String
    var vehicles = [Car]()
    
    init(_ location: String) {
        self.location = location
    }
    
    func loadVehicle(_ car : Car) {
        print("Loading vehicle \(car.name) from \(car.location)")
        vehicles.append(car)
    }
    
    func moveLocation(destination : String) {
        for item in vehicles {
            item.location = destination
        }
        
        self.location = destination
        
        print("Relocating to " + destination)
    }
    
    func offload() {
        if !vehicles.isEmpty {
            print("Offloading \(vehicles.popLast()!.name)")
        }
    }
    
}


var truck = VehicleTransporter("midrand")

truck.loadVehicle(benzo)

truck.loadVehicle(datsun)

truck.loadVehicle(bmw)

truck.moveLocation(destination: "Jozi")

truck.offload()


