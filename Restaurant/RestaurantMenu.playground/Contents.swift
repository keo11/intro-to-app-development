
struct Items {
    var name = ""
    var ingredients = [""]
    var price = 0.00
    var description = ""
    
    
    mutating func addIngredients(_ add: String) {
        ingredients.append(add)
    }
    
    var details : String {
        return "\(name) \t - \t R\(price) \n \(ingredients) \n *** \(description) *** \n "
    }
}

var menu = [Items]()

var item1 = Items(name : "Pizza", ingredients : ["Cheese, base, garlic, chicken, mushrooms, peppers"], price : 100.00, description: "Large chicken and mushroom pizza")

var item2 = Items(name : "Fish n chips", ingredients : ["Hake, onion rings, fries"], price : 50.00, description: "Medium hake, grilled or fried, with fried onion rings and chips")

item2.addIngredients("with medium coke")


var item3 = Items(name : "Sphatlho", ingredients : ["chips, sunnyside egg, cheese, letuce, russian, sliced steak bits, mini quarter loaf"], price : 50.00, description: "Iconic sandwich with a combination of fillings in a mini quarter of a white loaf of bread")

var item4 = Items(name : "Ribs n wings", ingredients : ["400g ribs, 4 wings, fries"], price : 70.00, description: "Juicy ribs and wings served with crispy chips")

var item5 = Items(name : "Grilled gizzards", ingredients : ["10 gizzards, onion rings, fries"], price : 30.00, description: "Peeled and cleaned chicken gizzards served with onion rings and chips")

menu.append(item1)
menu.append(item2)
menu.append(item3)
menu.append(item4)
menu.append(item5)



print("±±±±±±±±±±±±±±±±±±±±± Junk Food Park!!! ±±±±±±±±±±±±±±±±±±±±±±")

print("\n Menu:")

for item in menu {
    print("``````````````````````````````````````````````````````````````````````````````````")
    print(item.details)
    //print("````````````````````````````````````````````````````````````")
}
