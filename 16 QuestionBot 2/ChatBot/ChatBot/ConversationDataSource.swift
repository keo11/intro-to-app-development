import Foundation
class ConversationDataSource {
    
    /// The conversations history array
    var conversation = [Message]()
    
    
    /// The number of Messages in the conversation
    var messageCount: Int {return conversation.count}
    
    /// Add a new question to the conversation
    func add(question: String) {
        print("Asked to add question: \(question)")
        
        
        let message = Message(date: Date(), text: question, type: .question)
        
        
        conversation.append(message)
        

        
    }
    
    /// Add a new answer to the conversation
    func add(answer: String) {
        print("Asked to add answer: \(answer)")
        
        
        let message = Message(date: Date(), text: answer, type: .answer)
            
        conversation.append(message)
        

    }
    
    /// The Message at a specific point in the conversation
    func messageAt(index: Int) -> Message {
        print("Asking for message at index \(index)")
        
        
        return conversation[index]
        
        

    }
}
