//
//  ContentView.swift
//  1stTableViewApp
//
//  Created by Keo Shiko on 2020/12/04.
//  Copyright © 2020 Glucode. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, World!")
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
