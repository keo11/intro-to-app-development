//
//  ViewController.swift
//  AnimalSounds
//
//  Created by Keo Shiko on 2020/12/15.
//  Copyright © 2020 Glucode. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    let meowSound = SimpleSound(named: "meow")
    let woofSound = SimpleSound(named: "woof")
    let mooSound = SimpleSound(named: "moo")
    

    @IBOutlet weak var animalSoundLable: UILabel!
    
    @IBAction func catButtonTapped(_ sender: UIButton) {
        animalSoundLable.text = "Meow!"
        meowSound.play()
    }
    
    @IBAction func dogButtonTapped(_ sender: UIButton) {
        animalSoundLable.text = "Woof!"
        woofSound.play()
    }
    
    @IBAction func cowButtonTapped(_ sender: UIButton) {
        animalSoundLable.text = "Moo!"
        mooSound.play()
    }
    

    
}

