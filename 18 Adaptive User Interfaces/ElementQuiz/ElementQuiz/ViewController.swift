//
//  ViewController.swift
//  ElementQuiz
//
//  Created by Keo Shiko on 2020/12/15.
//  Copyright © 2020 Glucode. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    
    let elementList = ["Carbon", "Gold", "Chlorine", "Sodium"]
    
    var currentElementIndex = 0
    
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var answerLabel: UILabel!
    
    
    @IBAction func showAnswer(_ sender: UIButton) {
        answerLabel.text = elementList[currentElementIndex]
    }
    
    @IBAction func goToNextElement(_ sender: UIButton) {
        currentElementIndex += 1
        
        if currentElementIndex >= elementList.count{
            currentElementIndex = 0
        }
        updateElement()
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        updateElement()
    }
    
    func updateElement(){
        answerLabel.text = "?"
        
        let element = elementList[currentElementIndex]
        let image = UIImage(named: element)
        imageView.image = image 
    }


}

