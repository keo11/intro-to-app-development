import UIKit

var str = "Hello, playground"

enum Device {
    case headPhones, iPhone, macbook , watch
    
    var emoji: String {
        switch self {
        case .headPhones: return "🎧"
        case .iPhone: return "📲"
        case .macbook: return "👨🏾‍💻"
        case .watch: return "⌚️"
        }
    }
    
    var priceValue: Int {
        switch self {
        case .headPhones: return 1
        case .iPhone: return 3
        case .macbook: return 4
        case .watch: return 2
        }
    }
    
    func checkValue(_ compareTo: Device) -> Bool {
        return self.priceValue > compareTo.priceValue
    }
}3

var phone = Device.iPhone
var laptop = Device.macbook

phone.emoji

phone.checkValue(laptop)

