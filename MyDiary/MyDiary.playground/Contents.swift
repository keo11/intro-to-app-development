import UIKit

var str = "Hello, playground"

func wakeUp()
{
    print("\t Rise and shine! It's about to get lit")
}

func breakfast(){ print("\t Breakfast! Most important meal of the day")}
func travelToWork(){print("\t Going to work, use public transport if possible")}
func arriveAtWork(){print("\t Got to work, first up, coffee!!!")}
func classSession(){print("\t Meeting with the baddest team")}
func lunch(){print("\t Lunch time! Let's eat")}
func work(){print("\t Let's write some more code! :)")}
func travelHome(){print("\t E chaile! Time to go back home, traffic is the worst")}
func haveDinner(){print("\t Dinner time! ")}
func watchTV(){print("\t Netflix and chill")}
func sleep(){print("\t Let's get some sleep, more adventures coming tomorrow")}
func attend(){print("\t Townhall is compulsory for everyone, including Eben")}
func goOut(){print("\t Having some fun with friends")}

print("My Diary!!")
print("--------- Monday the second day of the week ---------- ")

print("06:00")
wakeUp()

print("06:30")
breakfast()

print("07:00")
travelToWork()

print("09:00")
arriveAtWork()

print("11:00")
classSession()

print("13:00")
lunch()

print("14:00")
work()

print("17:00")
travelHome()

print("19:00")
haveDinner()

print("20:00")
watchTV()

print("22:00")
sleep()


print("\n\n\n--------- Tuesday Choose day ---------- ")

print("06:00")
wakeUp()

print("06:30")
breakfast()

print("07:00")
travelToWork()

print("09:00")
arriveAtWork()

print("10:00")
attend()

print("13:00")
lunch()

print("14:00")
work()

print("17:00")
travelHome()

print("19:00")
haveDinner()

print("20:00")
watchTV()

print("22:00")
sleep()


print("\n\n\n--------- Wacky Wednesday ---------- ")

print("06:00")
wakeUp()

print("06:30")
breakfast()

print("07:00")
travelToWork()

print("09:00")
arriveAtWork()

print("11:00")
classSession()

print("13:00")
lunch()

print("14:00")
work()

print("17:00")
travelHome()

print("19:00")
haveDinner()

print("20:00")
watchTV()

print("22:00")
sleep()

print("\n\n\n--------- Throwback Thursday ---------- ")

print("06:00")
wakeUp()

print("06:30")
breakfast()

print("07:00")
travelToWork()

print("09:00")
arriveAtWork()

print("11:00")
classSession()

print("13:00")
lunch()

print("14:00")
work()

print("17:00")
travelHome()

print("19:00")
haveDinner()

print("20:00")
watchTV()

print("22:00")
sleep()

print("\n\n\n--------- Thank God it's Friday ---------- ")

print("06:00")
wakeUp()

print("06:30")
breakfast()

print("07:00")
travelToWork()

print("09:00")
arriveAtWork()

print("11:00")
classSession()

print("13:00")
lunch()

print("14:00")
work()

print("17:00")
travelHome()

print("18:00")
goOut()


print("22:00")
sleep()
